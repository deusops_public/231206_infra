terraform {
  required_providers {
    yandex = {
      source = "yandex-cloud/yandex"
    }
  }
  required_version = ">= 0.13"
}

provider "vault" {
  address = "http://192.168.1.203:8200"
  skip_tls_verify = true
}

#########################################################################################################################################################################
######################################################################## HASHICORP VAULT SECRETS ########################################################################
#########################################################################################################################################################################

data "vault_generic_secret" "yc-secrets" {
  path = "deusops/yc-secrets"
}

provider "yandex" {
  token     = data.vault_generic_secret.yc-secrets.data["YC_TOKEN"]
  cloud_id  = data.vault_generic_secret.yc-secrets.data["YC_CLOUD_ID"]
  folder_id = data.vault_generic_secret.yc-secrets.data["YC_FOLDER_ID"]
  zone      = "ru-central1-a"
}

data "yandex_compute_image" "img" {
  image_id = data.vault_generic_secret.yc-secrets.data["IMAGE_ID"]
}

#########################################################################################################################################################################
############################################################################## File01 ###################################################################################
#########################################################################################################################################################################

resource "yandex_compute_instance" "file01" {
  name        = "file01"
  platform_id = "standard-v1"
  zone        = "ru-central1-a"

  resources {
    cores  = 2
    memory = 2
  }

  boot_disk {
    initialize_params {
      image_id = data.yandex_compute_image.img.id
      size     = 15
    }
  }

  network_interface {
    subnet_id = yandex_vpc_subnet.my-sn-1.id
    nat       = true
  }

  metadata = {
    user-data = "${file("./metadata.txt")}"
  }

  provisioner "file" {
    source      = "/home/rh/.bashrc"
    destination = "/home/rh/.bashrc"

    connection {
      type        = "ssh"
      user        = "rh"
      private_key = file("~/.ssh/id_ed25519")
      host        = self.network_interface[0].nat_ip_address
    }
  }

  ##remote+local exec
  provisioner "remote-exec" {
    inline = [
      "sudo hostnamectl set-hostname file01",
      "cd /home/rh/",
      "git clone https://gitlab.com/deusops_public/231206_service.git",
      "sudo cp /home/rh/231206_service/01-netcfg.yaml /etc/netplan/01-netcfg.yaml",
      "sudo netplan apply",
      "ansible-galaxy role install lexshabalin.231206_nfs_server_role",
      "ansible-galaxy collection install community.general",
      "ansible-galaxy collection install ansible.posix",
      "ansible-playbook -b -i localhost /home/rh/231206_service/nfs_server_playbook.yml  -vv"
    ]

    connection {
      type        = "ssh"
      user        = "rh"
      private_key = file("~/.ssh/id_ed25519")
      host        = self.network_interface[0].nat_ip_address
    }
  }

  scheduling_policy {
    preemptible = true
  }
}

#########################################################################################################################################################################
################################################################################# Web01 #################################################################################
#########################################################################################################################################################################

resource "yandex_compute_instance" "web01" {
  name        = "web01"
  platform_id = "standard-v1"
  zone        = "ru-central1-a"

  resources {
    cores  = 2
    memory = 2
  }

  boot_disk {
    initialize_params {
      image_id = data.yandex_compute_image.img.id
      size     = 15
    }
  }

  network_interface {
    subnet_id = yandex_vpc_subnet.my-sn-1.id
    nat       = true
  }

  metadata = {
    user-data = "${file("./metadata.txt")}"
  }

  provisioner "file" {
    source      = "/home/rh/.bashrc"
    destination = "/home/rh/.bashrc"

    connection {
      type        = "ssh"
      user        = "rh"
      private_key = file("~/.ssh/id_ed25519")
      host        = self.network_interface[0].nat_ip_address
    }
  }

  ##remote+local exec
  provisioner "remote-exec" {
    inline = [
      "sudo hostnamectl set-hostname web01",
      "cd /home/rh/",
      "git clone https://gitlab.com/deusops_public/231206_service.git",
      "sudo cp /home/rh/231206_service/01-netcfg.yaml /etc/netplan/01-netcfg.yaml",
      "sudo netplan apply",
      "ansible-galaxy collection install community.general",
      "ansible-galaxy collection install ansible.posix",
      "ansible-galaxy role install lexshabalin.231206_nginx_role",
      "ansible-galaxy role install lexshabalin.231206_nfs_client_role",
      "ansible-galaxy role install lexshabalin.231206_gitlab_runner_role",
#      "ansible-galaxy role install lexshabalin.231206_docker_role",
      "git clone https://gitlab.com/deusops_public/231206_service.git",
      "ansible-playbook -b -i localhost /home/rh/231206_service/nfs_client_playbook.yml  -vv"
    ]

    connection {
      type        = "ssh"
      user        = "rh"
      private_key = file("~/.ssh/id_ed25519")
      host        = self.network_interface[0].nat_ip_address
    }
  }

  scheduling_policy {
    preemptible = true
  }
  depends_on = [yandex_compute_instance.file01]
}

#########################################################################################################################################################################                                                  
############################################################################ OTHER STUFF ################################################################################
#########################################################################################################################################################################

############################################ NETWORK
resource "yandex_vpc_network" "my-nw-1" {
  name = "my-nw-1"
}

resource "yandex_dns_zone" "zone1" {
  name        = "my-public-zone"
  description = "desc"

  labels = {
    label1 = "label-1-value"
  }

  zone   = "shabalin.site."
  public = true
}

############################################ DNS

#DNS-overall
resource "yandex_dns_recordset" "rs1" {
  zone_id = yandex_dns_zone.zone1.id
  name    = "srv.shabalin.site."
  type    = "A"
  ttl     = 200
  data    = ["10.1.0.1"]
}

#DNS for file01
resource "yandex_dns_recordset" "file01" {
  zone_id = yandex_dns_zone.zone1.id
  name    = "www.file01"
  type    = "A"
  ttl     = 200
  data    = [yandex_compute_instance.file01.network_interface.0.nat_ip_address]
}

#DNS for web01
resource "yandex_dns_recordset" "web01" {
  zone_id = yandex_dns_zone.zone1.id
  name    = "www.web01"
  type    = "A"
  ttl     = 200
  data    = [yandex_compute_instance.web01.network_interface.0.nat_ip_address]
}

############################################ SUBNET
resource "yandex_vpc_subnet" "my-sn-1" {
  zone           = "ru-central1-a"
  network_id     = yandex_vpc_network.my-nw-1.id
  v4_cidr_blocks = ["192.168.10.0/24"]
}

#########################################################################################################################################################################                                                  
################################################################################# OUTPUT ################################################################################
#########################################################################################################################################################################

###EXTERNAL

output "external_ip_address_file01" {
  value = yandex_compute_instance.file01.network_interface.0.nat_ip_address
}

output "external_ip_address_web01" {
  value = yandex_compute_instance.web01.network_interface.0.nat_ip_address
}

###INTERNAL

output "internal_ip_address_file01" {
  value = yandex_compute_instance.file01.network_interface.0.ip_address
}

output "internal_ip_address_web01" {
  value = yandex_compute_instance.web01.network_interface.0.ip_address
}

