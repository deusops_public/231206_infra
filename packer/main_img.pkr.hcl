source "yandex" "main_img" {
  folder_id           = "b1gqhaovf5mlgu9qrsii"
  source_image_family = "ubuntu-2004-lts"
  ssh_username        = "ubuntu"
  use_ipv4_nat        = "true"
  image_description   = "Yandex Cloud main image"
  image_family        = "my-images"
  image_name          = "main-img"
  subnet_id           = "e9b7h9gd2c6h1ukfm7h8"
  disk_type           = "network-hdd"
  zone                = "ru-central1-a"
}

build {
  sources = ["source.yandex.main_img"]

  provisioner "file" {
    source = "/home/rh/.bashrc"
    destination = "/tmp/.bashrc"
} 

  provisioner "shell" {
    inline = [

      # Create dir for file provisioner
      "sudo mkdir /opt/cfg",
      "sudo cp /tmp/.bashrc /etc/bash.bashrc",

      # For gitlab runner
#      "curl -L 'https://packages.gitlab.com/install/repositories/runner/gitlab-runner/script.deb.sh' | sudo bash",

      # Global Ubuntu things
      "sudo apt-get update",
      "echo 'debconf debconf/frontend select Noninteractive' | sudo debconf-set-selections",
      "sudo apt-get install -y unzip python3-pip python3.8-venv gitlab-runner postgresql postgresql-contrib libpq-dev openjdk-17-jdk",

      # Ansible install
      "sudo curl https://bootstrap.pypa.io/get-pip.py -o get-pip.py",
      "python3 get-pip.py --user",
      "python3 -m pip install --user ansible",
      "sudo apt install ansible ca-certificates curl git gnupg lsb-release unzip mc -y",

      # Docker
      "sudo apt-get update",
      "sudo apt-get install -y docker.io docker-compose",
      "sudo usermod -aG docker $USER",
      "sudo chmod 666 /var/run/docker.sock",
      "sudo useradd -m -s /bin/bash -G docker yc-user"

    ]
  }
}
